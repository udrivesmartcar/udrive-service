class AddBookingTypeAndTimeToCarBookings < ActiveRecord::Migration
  def change
    add_column :car_bookings, :booking_type, :string
    add_column :car_bookings, :booking_time, :datetime
  end
end
